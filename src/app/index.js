import _ from 'lodash';

const component = () => {
	const element = document.createElement('div');

	element.innerHTML = _.join(['Hello', '!'], ' ');

	return element;
};

document.body.appendChild(component());
